﻿using System;
using UnityEngine;
using System.Collections;

public static class CSVUtils
{
    public static string[][] Parse(string text)
    {
        var rows = text.Split(new string[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
        var data = new string[rows.Length][];

        for (int i = 0; i < rows.Length; i++)
        {
            data[i] = rows[i].Split(',');
        }

        return data;
    }
}
