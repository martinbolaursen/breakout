﻿using UnityEngine;
using System.Collections;

public class BallBehaviour : MonoBehaviour
{
    private static int totalBalls;

    [SerializeField]
    private Vector2 moveDirection = Vector2.zero;

    [SerializeField]
    private Vector2 deflectAngle;

    [SerializeField]
    private float speed = 2f;

    private bool isColliding;

    private bool stasis = true;

    private TrailRenderer trail;

    private Animator anim;

    private void Awake()
    {
        trail = GetComponent<TrailRenderer>();
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        totalBalls++;
    }

	private void Update()
	{
        Fire();
        transform.Translate(moveDirection.normalized * speed * Time.deltaTime, Space.World);
	    isColliding = false;
        CheckForLoss();
        ClampPosition();
	}

    private void Fire()
    {
        if (stasis && Input.GetMouseButtonUp(0))
        {
            stasis = false;
            var min = new Vector2(-1, 1);
            var max = new Vector2(1, 1);
            moveDirection = Vector2.Lerp(min, max, Random.value);
            trail.enabled = true;
            anim.enabled = false;
        }
    }

    private void CheckForLoss()
    {
        if (transform.position.y > -5)
            return;

        totalBalls--;
        Destroy(gameObject);
        AudioManager.Play("LostBall");

        if (totalBalls <= 0)
            GameManager.Instance.LoseLive();
    }

    private void ClampPosition()
    {
        Vector2 position = transform.position;
        position.x = Mathf.Clamp(position.x, -7.75f, 7.75f);
        position.y = Mathf.Min(position.y, 3.4f);
        transform.position = position;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (isColliding) return;
        isColliding = true;
        Collider2D other = col.collider;

        if (other.CompareTag("Paddle"))
        {
            Deflect(col.contacts[0].point, other.bounds);
            other.GetComponent<PaddleBehaviour>().Bounce();
            AudioManager.Play("PaddleHit");
        }
        else
        {
            Reflect(col.contacts[0].normal);
            AudioManager.Play("BrickHit");
        }

        BaseBrick baseBrick = other.GetComponent<BaseBrick>();
        if (baseBrick != null)
        {
            baseBrick.Trigger();
        }

        IncreaseSpeed();
    }

    private void Deflect(Vector2 hitPoint, Bounds bounds)
    {
        float distanceToCenter = Mathf.Abs(hitPoint.x - bounds.center.x);
        float fractionOfExtents = distanceToCenter / bounds.extents.x;

        Vector2 from = Vector2.up;
        Vector2 to = deflectAngle;

        if (hitPoint.x < bounds.center.x)
            to.x = -to.x;

        moveDirection = Vector3.Lerp(from, to, fractionOfExtents);
    }

    private void Reflect(Vector3 normal)
    {
        moveDirection = Vector3.Reflect(moveDirection, normal);
    }

    private void IncreaseSpeed()
    {
        speed = Mathf.Min(speed + 0.1f, 10);
    }

    private void OnEnable()
    {
        GameManager.Instance.GameOver += OnGameOverHandler;
    }

    private void OnDisable()
    {
        GameManager.Instance.GameOver -= OnGameOverHandler;
    }

    private void OnGameOverHandler()
    {
        Destroy(gameObject);
    }
}