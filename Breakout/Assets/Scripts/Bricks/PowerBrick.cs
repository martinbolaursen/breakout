﻿using UnityEngine;
using System.Collections;

public class PowerBrick : BaseBrick
{
    [SerializeField]
    private GameObject[] powerUps;

    public override void Trigger()
    {
        base.Trigger();
        Spawn();
    }

    private void Spawn()
    {
        int random = Random.Range(0, powerUps.Length);
        Instantiate(powerUps[random], transform.position, Quaternion.identity);
    }
}
