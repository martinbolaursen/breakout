﻿using System;
using UnityEngine;
using System.Collections;

public class BaseBrick : MonoBehaviour
{
    private static int bricksLeft;

    [SerializeField]
    protected int health;

    protected Animator anim;

    protected new Collider2D collider;

    protected virtual void Awake()
    {
        anim = GetComponent<Animator>();
        collider = GetComponent<Collider2D>();
    }

    protected virtual void Start()
    {
        bricksLeft++;
        Animate();
    }

    public virtual void Trigger()
    {
        health--;
        Animate();
        if (health <= 0)
        {
            collider.enabled = false;
        }
        GameManager.Instance.Score += 5;
    }

    public void Kill()
    {
        health = 1;
        Trigger();
        //Animate();
        //collider.enabled = false;
        //GameManager.Instance.Score += 5;
    }

    protected virtual void Animate()
    {
        anim.SetInteger("health", health);
    }

    protected virtual void Remove()
    {
        bricksLeft--;
        Destroy(gameObject);
        if (bricksLeft <= 0)
        {
            GameManager.Instance.OnGameOver();
        }
    }
}
