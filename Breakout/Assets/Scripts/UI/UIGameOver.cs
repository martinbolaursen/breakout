﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIGameOver : MonoBehaviour
{
    [SerializeField]
    private GameObject header;

    [SerializeField]
    private GameObject newHighscore;

    [SerializeField]
    private Text score;

    private string highscorePath = "BreakoutByMartin_Highscore";

    private bool hasEnded;

    private void OnEnable()
    {
        GameManager.Instance.GameOver += GameOverHandler;
    }

    private void OnDisable()
    {
        GameManager.Instance.GameOver -= GameOverHandler;
    }

    private void GameOverHandler()
    {
        GetAndUpdateHighscore();
        header.SetActive(true);
    }

    private void GetAndUpdateHighscore()
    {
        hasEnded = true;
        int previous = PlayerPrefs.GetInt(highscorePath, 0);
        int current = GameManager.Instance.Score;
        if (current > previous)
        {
            newHighscore.SetActive(true);
            PlayerPrefs.SetInt(highscorePath, current);
            PlayerPrefs.Save();
            score.text = current.ToString();
        }
        else
        {
            score.text = previous.ToString();
        }
    }

    public void Continue()
    {
        Application.LoadLevel("Menu");
    }
}
