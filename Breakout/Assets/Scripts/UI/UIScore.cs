﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScore : MonoBehaviour
{
    [SerializeField]
    private Sprite[] numberSprites;

    private Image[] display;

	private void Start()
	{
        display = new Image[transform.childCount];
	    int childCount = transform.childCount;
	    for (int i = 0; i < childCount; i++)
	    {
	        display[childCount - 1 - i] = transform.GetChild(i).GetComponent<Image>();
	    }
	}
	
	private void Update()
	{
	    SetScore();
	}

    private void SetScore()
    {
        int score = GameManager.Instance.Score;
        int[] digits = GetDigits(score, 4);
        for (int i = 0; i < display.Length; i++)
        {
            display[i].sprite = numberSprites[digits[i]];
        }
    }

    private static int[] GetDigits(int value, int numberOfDigits)
    {
        int size = numberOfDigits;
        int[] digits = new int[size];
        for (int index = size - 1; index >= 0; index--)
        {
            digits[index] = value % 10;
            value = value / 10;
        }
        return digits;
    }
}
