﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UILives : MonoBehaviour
{
    private Image[] images;

    private void Awake()
    {
        images = new Image[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            images[i] = transform.GetChild(i).GetComponent<Image>();
        }
    }

    private void Start()
    {
        SetLives(GameManager.Instance.Lives);
    }

    private void OnEnable()
    {
        GameManager.Instance.Loss += SetLives;
    }

    private void OnDisable()
    {
        GameManager.Instance.Loss -= SetLives;
    }

    private void SetLives(int lives)
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].enabled = i < lives;
        }
    }
}
