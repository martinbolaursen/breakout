﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BrickType { Weak, Medium, Strong, Power }

public class BrickFactory : MonoBehaviour
{
    public static BrickFactory Instance { get; private set; }

    [SerializeField]
    private GameObject[] brickPrefabs;

    private Dictionary<BrickType, GameObject> bricks;

    private void Awake()
    {
        InitSingleton();
        LoadPrefabs();
    }

    private void InitSingleton()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void LoadPrefabs()
    {
        bricks = new Dictionary<BrickType, GameObject>(brickPrefabs.Length);
        for (int i = 0; i < brickPrefabs.Length; i++)
        {
            bricks.Add((BrickType)i, brickPrefabs[i]);
        }
    }

    public GameObject Create(string typeData)
    {
        BrickType type = TypeFromString(typeData);
        if (bricks.ContainsKey(type))
        {
            return Instantiate(bricks[type]);
        }
        return Instantiate(bricks[0]);
    }

    private static BrickType TypeFromString(string data)
    {
        int number;
        if (Int32.TryParse(data, out number))
        {
            switch (number)
            {
                case 2:
                    return BrickType.Medium;
                case 3:
                    return BrickType.Strong;
            }
        }

        switch (data)
        {
            case "x":
                return BrickType.Power;
        }
        return BrickType.Weak;
    }
}