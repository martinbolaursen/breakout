﻿using UnityEngine;
using System.Collections;

public class LightningPower : Power
{
    [SerializeField]
    private LayerMask layerMask;

    [SerializeField]
    private GameObject effect;

    public override void Apply(PaddleBehaviour paddle)
    {
        base.Apply(paddle);
        SpawnEffect();
        RemoveBlocks();
        AudioManager.Play("Lightning");
    }

    private void SpawnEffect()
    {
        Vector2 position = transform.position;
        position.y = 5;
        GameObject clone = (GameObject) Instantiate(effect, position, Quaternion.identity);
    }

    private void RemoveBlocks()
    {
        Vector2 origin = transform.position;
        origin.y = 5;

        RaycastHit2D[] hits = Physics2D.RaycastAll(origin, -Vector2.up, 10f, layerMask);
        foreach (RaycastHit2D rayHit in hits)
        {
            rayHit.collider.GetComponent<BaseBrick>().Kill();
        }
    }
}
