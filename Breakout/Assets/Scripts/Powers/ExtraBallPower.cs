﻿using UnityEngine;
using System.Collections;

public class ExtraBallPower : Power
{
    public override void Apply(PaddleBehaviour paddle)
    {
        base.Apply(paddle);
        GameManager.Instance.SpawnBall();
        AudioManager.Play("PowerGood");
    }
}
