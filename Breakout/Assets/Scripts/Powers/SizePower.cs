﻿using UnityEngine;
using System.Collections;

public class SizePower : Power
{
    [SerializeField]
    private bool isPositive = true;

    public override void Apply(PaddleBehaviour paddle)
    {
        base.Apply(paddle);

        var scale = paddle.transform.localScale;
        if (isPositive)
        {
            scale.x = Mathf.Min(scale.x * 2, 6);
            AudioManager.Play("PowerGood");
        }
        else
        {
            scale.x = Mathf.Max(scale.x * 0.5f, 0.75f);
            AudioManager.Play("PowerBad");
        }
        paddle.transform.localScale = scale;
    }
}
