﻿using UnityEngine;
using System.Collections;

public abstract class Power : MonoBehaviour
{
    private new Rigidbody2D rigidbody;

    private new Collider2D collider;

    private Animator anim;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        rigidbody.AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(0.2f, 1f)) * 7.5f, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("Paddle"))
        {
            var paddle = col.collider.GetComponent<PaddleBehaviour>();
            Apply(paddle);
            GameManager.Instance.Score += 100;
        }
    }

    public virtual void Apply(PaddleBehaviour paddle)
    {
        rigidbody.isKinematic = true;
        collider.enabled = false;
        anim.SetTrigger("Remove");
    }

    public virtual void Remove()
    {
        Destroy(gameObject);
    }
}
