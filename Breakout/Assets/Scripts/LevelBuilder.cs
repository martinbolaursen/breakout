﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelBuilder : MonoBehaviour
{
    [SerializeField]
    private Vector2 levelBounds;

    [SerializeField]
    private float brickHeight = 1f;

    private string[][] levelData;

	private void Start()
	{
        LoadLevelData();
        BuildLevel();
	}

    private void LoadLevelData()
    {
        string rawLevelData = Resources.Load<TextAsset>("level").text;
        levelData = CSVUtils.Parse(rawLevelData); 
    }

    private void BuildLevel()
    {
        for (int i = 0; i < levelData.Length; i++)
        {
            BuildRow(i, levelData[i]);
        }
    }

    private void BuildRow(int rowIndex, string[] rowData)
    {
        Vector2 offset = levelBounds * 0.5f;
        Vector2 position = new Vector2(transform.position.x - offset.x, transform.position.y + offset.y - (rowIndex*brickHeight));
        float brickWidth = levelBounds.x / rowData.Length;

        for (int i = 0; i < rowData.Length; i++)
        {
            var brick = BrickFactory.Instance.Create(rowData[i]);
            brick.transform.localScale = new Vector2(brickWidth, brickHeight);
            brick.transform.position = position;
            brick.transform.parent = transform;
            position.x += brickWidth;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, new Vector2(levelBounds.x, levelBounds.y));
    }
}
