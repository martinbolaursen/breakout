﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(SFX))]
public class SFXDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty id = property.FindPropertyRelative("Id");
        SerializedProperty clip = property.FindPropertyRelative("Clip");

        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), new GUIContent("SFX"));

        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect idLabelRect = new Rect(position.x - 20, position.y, 50, position.height);
        Rect idRect = new Rect(position.x, position.y, position.width*0.5f - 35, position.height);

        EditorGUI.LabelField(idLabelRect, "Id:");
        EditorGUI.PropertyField(idRect, id, GUIContent.none);

        Rect clipLabelRect = new Rect(position.x + position.width*0.5f + 5, position.y, 50, position.height);
        Rect clipRect = new Rect(position.x + position.width*0.5f + 35, position.y, position.width*0.5f - 35, position.height);

        EditorGUI.LabelField(clipLabelRect, "Clip:");
        EditorGUI.PropertyField(clipRect, clip, GUIContent.none);

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }
}
