﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class SetActiveEditor : EditorWindow
{
    [MenuItem("Muton/ToggleActive %q")]
    public static void ToggleActiveSelected()
    {
        var selection = Selection.gameObjects;
        if (selection.Length > 0)
        {
            var state = Selection.gameObjects[0].activeSelf;
            foreach (var go in selection)
            {
                go.SetActive(!state);
            }
        }
    }
}