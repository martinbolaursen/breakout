﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class SFX
{
    public string Id;
    public AudioClip Clip;

    public SFX(string id, AudioClip clip)
    {
        this.Id = id;
        this.Clip = clip;
    }
}

public class AudioManager : MonoBehaviour
{
    private static AudioManager Instance { get; set; }

    [SerializeField]
    private AudioClip music;

    [SerializeField]
    private SFX[] soundsToLoad;

    private AudioSource musicSource;

    private Dictionary<string, AudioClip> sounds; 

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        musicSource = gameObject.AddComponent<AudioSource>();

        LoadSounds();
        PlayMusic();
    }

    private void LoadSounds()
    {
        sounds = new Dictionary<string, AudioClip>(soundsToLoad.Length);
        for (int i = 0; i < soundsToLoad.Length; i++)
        {
            SFX sfx = soundsToLoad[i];
            if (sounds.ContainsKey(sfx.Id))
                continue;
            sounds.Add(sfx.Id, sfx.Clip);
        }
    }

    private void PlayMusic()
    {
        if (music == null)
            return;
        musicSource.clip = music;
        musicSource.loop = true;
        musicSource.volume = 0.5f;
        musicSource.Play();
    }

    public static void Play(string id)
    {
        Instance.PlayInternal(id);
    }

    private void PlayInternal(string id)
    {
        if (Exists(id))
        {
            musicSource.PlayOneShot(sounds[id], 1f);
        }
    }

    private bool Exists(string id)
    {
        return sounds.ContainsKey(id);
    }
}
