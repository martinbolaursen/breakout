﻿using System;
using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public event Action<int> Loss;

    public event Action GameOver;

    public static GameManager Instance { get; private set; }
    
    [SerializeField]
    private int powersLayer, ballsLayer, bricksLayer;

    [SerializeField]
    private GameObject ballPrefab;

    public int Score {get; set; }

    public int Lives { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        Lives = 3;
    }

    private void Start()
    {
        SetupCollisions();
    }

    private void SetupCollisions()
    {
        Physics2D.IgnoreLayerCollision(powersLayer, ballsLayer);
        Physics2D.IgnoreLayerCollision(powersLayer, bricksLayer);
    }

    public void LoseLive()
    {
        Lives--;
        OnLoss(Lives);
        if (Lives >= 0)
        {
            SpawnBall();
        }
        else
        {
            OnGameOver();
        }
    }

    public void SpawnBall()
    {
        Instantiate(ballPrefab);
    }

    private void OnLoss(int lives)
    {
        var handler = Loss;
        if (handler != null) handler(lives);
    }

    public void OnGameOver()
    {
        var handler = GameOver;
        if (handler != null) handler();
    }
}
