﻿using UnityEngine;
using System.Collections;

public class PaddleBehaviour : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 1f;

    [SerializeField]
    private float offset = 0f;

    private float constrains;

    private Animator anim;

    private BoxCollider2D collider;

    public Bounds Bounds
    {
        get { return collider.bounds; }
    }

    public void Bounce()
    {
        anim.SetTrigger("Hit");
    }

    private void Awake()
    {
        anim = GetComponent<Animator>();
        collider = GetComponent<BoxCollider2D>();
    }

	private void Start()
	{
	    constrains = Camera.main.ScreenToWorldPoint(Vector2.zero).x;
	}
	
	private void Update()
	{
        Move();
	}

    private void Move()
    {
        Vector2 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float clampValue = constrains + collider.bounds.extents.x;
        target.x = Mathf.Clamp(target.x, clampValue + offset, -clampValue - offset);
        target.y = transform.position.y;
        transform.position = Vector3.Lerp(transform.position, target, moveSpeed*Time.deltaTime);
    }

    private void OnEnable()
    {
        GameManager.Instance.GameOver += OnGameOverHandler;
    }

    private void OnDisable()
    {
        GameManager.Instance.GameOver -= OnGameOverHandler;
    }

    private void OnGameOverHandler()
    {
        Destroy(gameObject);
    }
}
